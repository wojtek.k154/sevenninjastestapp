class SessionsController < ApplicationController
  protect_from_forgery with: :null_session
  def new
    @user = User.new
  end

  def create
    if env["omniauth.auth"]
      @user = User.from_omniauth(env["omniauth.auth"])
      if @user && @user.oauth_token.eql?(env["omniauth.auth"]["credentials"]["token"])
        cookies[:auth_token] = @user.auth_token
        flash[:notice] = "Signed in successfully!"
        redirect_to root_url
      else
        flash[:alert] = "Occured errors with login with facebook!"
        render :new
      end

    else
      @user = User.find_by_email(session_params[:email])
      if @user && @user.authenticate(session_params[:password])
        if session_params[:remember_me].eql?(1)
          cookies[:auth_token] = { value: @user.auth_token, expires: 2.weeks.from_now }
        else
          cookies[:auth_token] = @user.auth_token
        end
        flash[:notice] = "Signed in successfully!"
        redirect_to root_url
      else
        render :new
        flash[:alert] = "Invalid email or password"
      end
    end
  end

  def destroy
    cookies.delete(:auth_token)
    reset_session
    redirect_to root_path, notice: "You have been logged out."
  end
  private
    def session_params
      params.require(:user).permit(:email, :password, :remember_me, :provider, :uid, :oauth_token, :name, :surname  )
    end
end
