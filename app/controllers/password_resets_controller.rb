class PasswordResetsController < ApplicationController
  def new
    @user = User.new
  end
  def create
    @user = User.find_by_email(reset_params[:email])
    @user.send_password_reset if @user
    @user.save
    redirect_to root_url, notice: "Email sent with password reset instructions."
  end

  def edit
    @user = User.find_by_password_reset_token(params[:password_reset_token])
  end

  def update
    @user = User.find_by_password_reset_token(reset_params[:password_reset_token])
    @user.password = reset_params[:password]
    if @user.save
      redirect_to root_url, :notice => "Password has been reset!"
    elsif @user.password_reset_sent_at < 2.hours.ago
      redirect_to new_password_reset_path, :alert => "Password reset has expired."
    else
      render :edit
    end
  end

  private
    def reset_params
      params.require(:user).permit(:email, :password, :password_confirmation, :password_reset_token)
    end
end
