class RegistrationsController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(registrations_params)
    @user.password = registrations_params[:password]
    if @user.save && verify_recaptcha(@user)
      redirect_to root_url
      session[:user_id] = @user.id
      flash[:notice] = "Thank you for sign up!"
    else
      render :new
      flash[:alert] = "Wrong filled form! Fix it and try agin!"
    end
  end
  private
    def registrations_params
      params.require(:user).permit(:email, :name, :surname, :password, :password_confirmation)
    end
end
