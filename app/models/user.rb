class User < ActiveRecord::Base
  attr_accessor :password, :password_confirmation, :remember_me
  before_save :encrypt_password
  after_save :clear_password
  before_create { generate_token(:auth_token) }

  def encrypt_password 
    if password
      self.salt = BCrypt::Engine.generate_salt
      self.password_digest = BCrypt::Engine.hash_secret(password, salt)
    end
  end

  def clear_password
    self.password = nil
  end

  def authenticate(password)
    password_digest.eql?(BCrypt::Engine.hash_secret(password, salt)) ? true : false
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.current
    UserMailer.password_reset(self).deliver
  end

  def self.from_omniauth(auth)
    unless where(email: auth.info.email).empty?
      user = where(email: auth.info.email).first
      user.provider = auth.provider
      user.uid = auth.uid
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
      user
    else
      where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |user|
        user.email = auth.info.email
        user.name = auth.info.name.split(' ')[0]
        user.surname = auth.info.name.split(' ')[1]
        user.provider = auth.provider
        user.uid = auth.uid
        user.oauth_token = auth.credentials.token
        user.oauth_expires_at = Time.at(auth.credentials.expires_at)
        user.save!
        user
      end
    end
  end

  validates :email, presence: true, uniqueness: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
  validates_confirmation_of :password
  validates :password, length: 6..20, format: { with: /^[a-zA-Z0-9]+$/, multiline: true }, allow_blank: true
  validates :name, :surname, presence: true
end
