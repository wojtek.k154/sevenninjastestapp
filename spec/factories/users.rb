FactoryGirl.define do
  factory :user do
    email "wojtek.kustrzyk@gmail.com"
    name "Wojciech"
    surname "Kustrzyk"
    password "test1234"
    password_confirmation "test1234"
  end
end
