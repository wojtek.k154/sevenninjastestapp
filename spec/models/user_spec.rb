require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryGirl.build(:user) }

  it "require email" do
    expect(user.email).to eq("wojtek.kustrzyk@gmail.com")
  end

  it "require name" do
    expect(user.name).to eq("Wojciech")
  end
  it "require surname" do
    expect(user.surname).to eq("Kustrzyk")
  end
  it "require password" do
    expect(user.password_confirmation).to eq("test1234")
  end
  it "require password_confirmation" do
    expect(user.password_confirmation).to eq("test1234")
  end
end
