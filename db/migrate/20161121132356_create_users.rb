class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email,              null: false, default: ""
      t.string :name,               null: false, default: ""
      t.string :surname,            null: false, default: ""
      t.string :password_digest
      t.string :salt 
      t.timestamps null: false
    end
  end
end
